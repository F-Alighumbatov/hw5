package com.company;

import java.util.Random;

public class Main {


    public static void main(String[] args) {
        Random random = new Random();
        Human human = new Human("tuesday");

        Pet pet = new Pet("dog", "Jackie", 7, 75, new String[]{"eat", "drink", "sleep"});
        Human mother = new Human("jane ", "Karleon", 1989);
        Human father = new Human("john", "Karleon", 1967);
        Human child = new Human("Jack", "Karleon", random.nextInt(100), random.nextInt(100), mother, father, pet, "saturday ", new String[7][2]);
        Human child1 = new Human("Adam", "Karleon", random.nextInt(100), random.nextInt(100), mother, father, pet, "saturday ", new String[7][2]);

        Human child2 = new Human("Jane", "Karleon", random.nextInt(100), random.nextInt(100), mother, father, pet, "saturday ", new String[7][2]);
        Human child3 = new Human("Ford", "Karleon", random.nextInt(100), random.nextInt(100), mother, father, pet, "saturday ", new String[7][2]);
        Human child4 = new Human("Allan", "Karleon", random.nextInt(100), random.nextInt(100), mother, father, pet, "saturday ", new String[7][2]);

        System.out.println("-------------------------------------------------");

        Family family = new Family(mother, father, new Human[]{child, child1});
        System.out.println(child.greetPet() + "\n"
                + child.describePet());
        System.out.println(child.feedPet());
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        System.out.println(family);
        System.out.println(family.countFamily());
        family.deleteChild(child1);
        System.out.println(family);
        System.out.println(family.countFamily());

    }

}


