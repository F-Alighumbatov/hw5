package com.company;

import java.util.*;

public class Family extends Human {

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    private Family family;


    public Family(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, String day, String[][] schedule) {
        super(name, surname, year, iq, mother, father, pet, day, schedule);

    }

    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;


    }

    public Family(Human mother, Human father, Human[] children, Family family) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.family = family;
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append(children != null ? "Human{" +
                Arrays.deepToString(children) +
                "}" : "");
        str.delete(5, 7);
        str.delete(str.length() - 3, str.length() - 1);
        return str.toString();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] child) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    @Override
    public String greetPet() {
        return super.greetPet();
    }

    @Override
    public String describePet() {
        return super.describePet();
    }

    @Override
    public boolean feedPet() {
        return super.feedPet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        if (!super.equals(o)) return false;
        Family family1 = (Family) o;
        return Objects.equals(getMother(), family1.getMother()) && Objects.equals(getFather(), family1.getFather()) && Arrays.equals(getChildren(), family1.getChildren()) && Objects.equals(getPet(), family1.getPet()) && Objects.equals(family, family1.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), getMother(), getFather(), getPet(), family.children);
        result = 31 * result + Arrays.hashCode(getChildren());
        return result;
    }

    public String countFamily() {
        int count = children.length + 2;
        return "The family members are: ".concat(String.valueOf(count));
    }

    public void addChild(Human human) {

        Human[] add = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            add[i] = children[i];
        }
        add[children.length] = human;
        this.children = add;

    }

    public boolean deleteChild(Human human) {
        boolean deleted = false;
        Human[] childArray = new Human[this.children.length - 1];
        int index = 0;
        for (Human child : this.children) {
            if (!child.equals(human)) {
                childArray[index++] = child;
            }
        }
        this.children = childArray;
        if (index < childArray.length - 1) {
            deleted = true;
        }
        return deleted;
    }


}
