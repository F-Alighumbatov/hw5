package com.company;

import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Human {
    Random random = new Random();
    private String name;
    private String surname;
    private int year;
    private String day;
    private int iq;
    private String childName;
    private Pet pet;
    private String[][] schedule = new String[7][2];

    public void setDay(String day) {
        this.day = day;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Human() {

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getDay() != null ? getDay() : "");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getName() {
        return name;
    }

    public Human(String day) {
        this.day = day;

    }


    public String getDay() {
        return day;
    }


    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;

    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, String day, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.day = day;

    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append(name != null && !name.isEmpty() ? "{name='" + name + "', " : "");
        str.append(surname != null && !surname.isEmpty() ? "surname='" + surname + "', " : "");
        str.append(year != 0 ? "year=" + year + ", " : "");
        str.append(iq != 0 ? "iq=" + iq + ", " : "");
        str.append("schedule=[");
        for (int i = 0; i < schedule().length - 1; i++) {
            for (int j = 0; j < schedule()[i].length; j++) {

                str.append(schedule()[i][j] != null ? "[" + schedule[i][j] + "]" : "");
            }
        }
        str.append("]");
        return str.toString();
    }

    public String[][] schedule() {

        String check;
        check = getDay();
        check = check.toUpperCase().trim();

        switch (check) {
            case "MONDAY": {
                schedule[0][0] = "Monday: ";
                schedule[0][1] = "go to courses; watch a film";
            }
            break;
            case "TUESDAY": {
                schedule[1][0] = "Tuesday: ";
                schedule[1][1] = "go to work";
            }

            break;
            case "WEDNESDAY": {
                schedule[2][0] = "Wednesday: ";
                schedule[2][1] = "do the exercises";
            }
            break;
            case "THURSDAY": {
                schedule[3][0] = "Thursday: ";
                schedule[3][1] = "go to the restaurant";
            }
            break;
            case "FRIDAY": {
                schedule[4][0] = "Friday: ";
                schedule[4][1] = "do homework";
            }
            break;
            case "SATURDAY": {
                schedule[5][0] = "Saturday: ";
                schedule[5][1] = "go to the theatre";
            }
            break;
            case "SUNDAY": {
                schedule[6][0] = "Sunday: ";
                schedule[6][1] = "meet with friend";
            }
            break;
//    }
        }
        return schedule;
    }

    public String greetPet() {
        return "Hello! ".concat(pet.getNickname());

    }

    public String describePet() {
        if (pet.getTrickLevel() > 50) {
            return "I have a ".concat(pet.getSpecies().concat(", he is ")
                    .concat(String.valueOf(pet.getAge())).concat(" years old,")
                    .concat("he is very sly"));
        } else if (pet.getTrickLevel() <= 50) {
            return "I have a ".concat(pet.getSpecies().concat(", he is ")
                    .concat(String.valueOf(pet.getAge())).concat(" years old,")
                    .concat("he is almost not sly"));
        }
        return describePet();
    }

    public boolean feedPet() {

        if (pet.getTrickLevel() > random.nextInt()) {
            System.out.println("Hmm...I will feed ".concat(pet.getNickname()).concat("'s "));
            return true;
        } else System.out.println("I think ".concat(pet.getNickname()).concat("'s ")
                .concat(" is not hungary"));

        return false;
    }

}