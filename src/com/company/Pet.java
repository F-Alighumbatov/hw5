package com.company;

import java.util.Arrays;

public class Pet {

    private String species;
    private String nickname = null;
    private int age;
    private int trickLevel;
    private String[] habits;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public int getAge() {
        return age;
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    @Override
    public String toString() {
        return species +
                "{nickname= " + "'" + nickname + "'" +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits);
    }


    public String eat() {
        return "I am eating";
    }

    public String respond() {
        return "Hello, owner. I am ".concat(getNickname()).concat(". I miss you!");
    }

    public String foul() {

        return "I need to cover it up";
    }


}
